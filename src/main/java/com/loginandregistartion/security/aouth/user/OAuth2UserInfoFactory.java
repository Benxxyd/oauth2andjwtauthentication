package com.loginandregistartion.security.aouth.user;

import java.util.Map;
import com.loginandregistartion.exceptions.OAuth2AuthenticationProcessingException;
import com.loginandregistartion.model.AuthProvider;

public class OAuth2UserInfoFactory {

  public static OAuth2UserInfo getOAuth2UserInfo(String registrationId,
      Map<String, Object> attributes) {
    if (registrationId.equalsIgnoreCase(AuthProvider.google.toString())) {
      return new GoogleOAuth2UserInfo(attributes);
    } else if (registrationId.equalsIgnoreCase(AuthProvider.facebook.toString())) {
      return new FacebookOAuth2UserInfo(attributes);
    } else {
      throw new OAuth2AuthenticationProcessingException(
          "Login with " + registrationId + " not supported.");
    }
  }
}
