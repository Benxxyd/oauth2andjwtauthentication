package com.loginandregistartion.security;


import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import com.loginandregistartion.dao.IUsersDAO;
import com.loginandregistartion.exceptions.ResourceNotFoundException;
import com.loginandregistartion.model.DBUser;


@Service
public class CustomUserDetailsService implements UserDetailsService {

  @Autowired
  IUsersDAO usersDAO;

  @Override
  @Transactional
  public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
    DBUser user = usersDAO.findByEmail(email)
        .orElseThrow(() -> new UsernameNotFoundException("User not found with email : " + email));

    return UserPrincipal.create(user);
  }

  @Transactional
  public UserDetails loadUserById(String id) {
    DBUser user =
        usersDAO.findById(id).orElseThrow(() -> new ResourceNotFoundException("User", "id", id));

    return UserPrincipal.create(user);
  }
}
