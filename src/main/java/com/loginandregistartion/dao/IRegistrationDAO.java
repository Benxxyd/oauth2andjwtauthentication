package com.loginandregistartion.dao;

import javax.transaction.Transactional;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.repository.CrudRepository;
import com.loginandregistartion.model.DBRegistrationData;

@Transactional
public interface IRegistrationDAO extends CrudRepository<DBRegistrationData, String> {

  @Modifying
  void deleteByUsername(String username);

}
