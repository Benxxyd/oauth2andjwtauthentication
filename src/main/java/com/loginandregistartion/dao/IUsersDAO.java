package com.loginandregistartion.dao;

import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import com.loginandregistartion.model.DBUser;


public interface IUsersDAO extends JpaRepository<DBUser, String> {

  Optional<DBUser> findByEmail(String email);

  Optional<DBUser> findByForgetPasswordToken(String forgetPasswordToken);
}
