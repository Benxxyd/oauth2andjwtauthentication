package com.loginandregistartion.dao;

import javax.transaction.Transactional;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import com.loginandregistartion.model.DBUserRole;

@Transactional
public interface IUserRolesDAO extends CrudRepository<DBUserRole, String> {

  @Modifying
  @Transactional
  @Query("delete from DBUserRole e where APP_USER_ID = ?1")
  void deleteByUserId(String userId);

}
