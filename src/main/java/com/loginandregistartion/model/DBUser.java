package com.loginandregistartion.model;

import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "APP_USER", uniqueConstraints = {@UniqueConstraint(columnNames = "email")})
public class DBUser {
  @Id
  @Column(name = "ID")
  @GeneratedValue(generator = "UUID")
  @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
  private String id;

  @Column
  private String name;

  @Email
  @Column(nullable = false)
  private String email;

  @Column(length = 500)
  private String imageUrl;

  @Column
  private Boolean emailVerified = false;

  @Column
  private String password;

  @Column
  private String providerId;

  @Column
  private String forgetPasswordToken;

  @NotNull
  @Enumerated(EnumType.STRING)
  private DBAuthProvider provider;

  @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true)
  @JoinColumn(name = "APP_USER_ID", referencedColumnName = "ID")
  private List<DBUserRole> roles;

  public DBUser() {}

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getImageUrl() {
    return imageUrl;
  }

  public void setImageUrl(String imageUrl) {
    this.imageUrl = imageUrl;
  }

  public Boolean getEmailVerified() {
    return emailVerified;
  }

  public void setEmailVerified(Boolean emailVerified) {
    this.emailVerified = emailVerified;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public DBAuthProvider getProvider() {
    return provider;
  }

  public void setProvider(DBAuthProvider provider) {
    this.provider = provider;
  }

  public List<DBUserRole> getRoles() {
    return roles;
  }

  public void setRoles(List<DBUserRole> roles) {
    this.roles = roles;
  }

  public String getProviderId() {
    return providerId;
  }

  public void setProviderId(String providerId) {
    this.providerId = providerId;
  }

  public String getForgetPasswordToken() {
    return forgetPasswordToken;
  }

  public void setForgetPasswordToken(String forgetPasswordToken) {
    this.forgetPasswordToken = forgetPasswordToken;
  }
}
