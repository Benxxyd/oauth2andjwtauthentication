package com.loginandregistartion.model;

import java.util.UUID;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import com.loginandregistartion.api.UserRegistrationRequest;
import com.loginandregistartion.api.UserRegistrationResponse;
import com.loginandregistartion.api.UserStateEnum;

public class UserRegistrationMapper {
  private static final BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();

  public static DBRegistrationData mapToDb(UserRegistrationRequest userRegistrationRequest) {
    DBRegistrationData dbRegistrationData = new DBRegistrationData();
    dbRegistrationData.setUsername(userRegistrationRequest.getEmail());
    dbRegistrationData.setDbUserStatusEnum(DBUserStatusEnum.WAITING_FOR_EMAIL_VERIFICATION);
    dbRegistrationData.setVerificationCode(UUID.randomUUID().toString());
    dbRegistrationData.setPassword(encoder.encode(userRegistrationRequest.getPassword()));

    return dbRegistrationData;
  }

  public static UserRegistrationResponse mapToApi(DBRegistrationData dbRegistrationData) {
    return new UserRegistrationResponse().id(dbRegistrationData.getId())
        .userState(UserStateEnum.valueOf(dbRegistrationData.getDbUserStatusEnum().toString()));
  }
}
