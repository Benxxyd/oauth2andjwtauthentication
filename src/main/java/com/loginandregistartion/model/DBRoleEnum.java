package com.loginandregistartion.model;

public enum DBRoleEnum {
    ADMIN, USER;
    
    public String authority() {
        return "ROLE_" + this.name();
    }
}
