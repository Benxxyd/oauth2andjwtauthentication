package com.loginandregistartion.model;

public enum DBAuthProvider {
    local,
    facebook,
    google
}
