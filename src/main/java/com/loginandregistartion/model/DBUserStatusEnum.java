package com.loginandregistartion.model;

public enum DBUserStatusEnum {
    WAITING_FOR_EMAIL_VERIFICATION, ACTIVE, DEACTIVATED;

}
