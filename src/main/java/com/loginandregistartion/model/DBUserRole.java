package com.loginandregistartion.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;

@Entity
@Table(name = "USER_ROLE")
public class DBUserRole {
  @Embeddable
  public static class Id implements Serializable {
    private static final long serialVersionUID = 1322120000551624359L;

    @Column(name = "APP_USER_ID")
    protected String userId;

    @Enumerated(EnumType.STRING)
    @Column(name = "ROLE")
    protected DBRoleEnum DBRoleEnum;


    public String getUserId() {
      return userId;
    }

    public void setUserId(String userId) {
      this.userId = userId;
    }

    public Id() {}

    public Id(String userId, DBRoleEnum DBRoleEnum) {
      this.userId = userId;
      this.DBRoleEnum = DBRoleEnum;
    }
  }

  public Id getId() {
    return id;
  }

  public void setId(Id id) {
    this.id = id;
  }

  public void setDBRoleEnum(com.loginandregistartion.model.DBRoleEnum DBRoleEnum) {
    this.DBRoleEnum = DBRoleEnum;
  }

  @EmbeddedId
  Id id = new Id();

  @Enumerated(EnumType.STRING)
  @Column(name = "ROLE", insertable = false, updatable = false)
  protected DBRoleEnum DBRoleEnum;

  public DBRoleEnum getDBRoleEnum() {
    return DBRoleEnum;
  }
}
