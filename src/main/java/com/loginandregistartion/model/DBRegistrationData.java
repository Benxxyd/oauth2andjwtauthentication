package com.loginandregistartion.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "REGISTRATION_DATA")
public class DBRegistrationData {
  @Id
  @Column(name = "ID")
  @GeneratedValue(generator = "UUID")
  @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
  private String id;

  @Column(name = "USERNAME")
  private String username;

  @Column(name = "PASSWORD")
  private String password;

  @Column(name = "VERIFICATION_CODE")
  private String verificationCode;

  @Enumerated(EnumType.STRING)
  @Column(name = "STATE")
  protected DBUserStatusEnum dbUserStatusEnum;

  public DBRegistrationData() {

  }

  public DBRegistrationData(String id, String username, String password, String verificationCode) {
    this.id = id;
    this.username = username;
    this.password = password;
    this.verificationCode = verificationCode;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public String getVerificationCode() {
    return verificationCode;
  }

  public void setVerificationCode(String verificationCode) {
    this.verificationCode = verificationCode;
  }

  public DBUserStatusEnum getDbUserStatusEnum() {
    return dbUserStatusEnum;
  }

  public void setDbUserStatusEnum(DBUserStatusEnum dbUserStatusEnum) {
    this.dbUserStatusEnum = dbUserStatusEnum;
  }
}
