package com.loginandregistartion.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.loginandregistartion.api.ForgetPasswordInput;
import com.loginandregistartion.api.ForgetPasswordVerificationInput;
import com.loginandregistartion.api.ForgetpasswordApi;
import com.loginandregistartion.services.IForgetPasswordService;
import io.swagger.annotations.ApiParam;


@Controller
public class ForgetpasswordApiController implements ForgetpasswordApi {

  @Autowired
  private IForgetPasswordService forgetPasswordService;

  @Autowired
  public ForgetpasswordApiController(ObjectMapper objectMapper) {}

  @Override
  public ResponseEntity<Void> completeForgetPassword(
      @ApiParam(value = "the forget password token",
          required = true) @PathVariable("forgetPasswordToken") String forgetPasswordToken,
      @ApiParam(value = "Forget password verification input.",
          required = true) @RequestBody ForgetPasswordVerificationInput forgetPasswordVerificationInput,
      @RequestHeader(value = "Accept", required = false) String accept) throws Exception {
    forgetPasswordService.confirmForgetPassword(forgetPasswordToken,
        forgetPasswordVerificationInput);
    return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
  }

  @Override
  public ResponseEntity<Void> initiateForgetPassword(
      @ApiParam(value = "Forget password input.",
          required = true) @RequestBody ForgetPasswordInput forgetPasswordInput,
      @RequestHeader(value = "Accept", required = false) String accept) throws Exception {
    forgetPasswordService.initiateForgetPassword(forgetPasswordInput);
    return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
  }

}
