package com.loginandregistartion.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestHeader;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.loginandregistartion.api.UserApi;
import com.loginandregistartion.api.UserData;
import com.loginandregistartion.security.CurrentUser;
import com.loginandregistartion.security.UserPrincipal;
import com.loginandregistartion.services.IUsersService;

/**
 * Api implemention
 *
 * @author pkmst
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaPKMSTServerCodegen",
    date = "2019-11-02T14:08:04.435Z")

@Controller
public class UserApiController implements UserApi {
  @Autowired
  private IUsersService usersService;

  @Autowired
  public UserApiController(ObjectMapper objectMapper) {}

  @Override
  @PreAuthorize("hasRole('USER')")
  public ResponseEntity<UserData> getUser(@CurrentUser UserPrincipal userPrincipal)
      throws Exception {
    return ResponseEntity.ok(usersService.getUserData(userPrincipal.getId()));
  }

  @Override
  public ResponseEntity<Void> deleteUserData(@CurrentUser UserPrincipal userPrincipal,
      @RequestHeader(value = "Accept", required = false) String accept) throws Exception {
    usersService.deleteUserData(userPrincipal.getId());
    return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
  }
}
