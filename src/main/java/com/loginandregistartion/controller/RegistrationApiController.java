package com.loginandregistartion.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.loginandregistartion.api.RegistrationApi;
import com.loginandregistartion.api.UserRegistrationRequest;
import com.loginandregistartion.api.UserRegistrationResponse;
import com.loginandregistartion.services.IUserRegistrationService;
import io.swagger.annotations.ApiParam;

/**
 * Api implemention
 * 
 * @author pkmst
 *
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaPKMSTServerCodegen",
    date = "2019-10-19T21:22:31.676Z")

@Controller
public class RegistrationApiController implements RegistrationApi {

  @Autowired
  private IUserRegistrationService userRegistrationService;

  @Autowired
  public RegistrationApiController(ObjectMapper objectMapper) {}

  @Override
  public ResponseEntity<UserRegistrationResponse> completeRegistration(
      @ApiParam(value = "the registration id",
          required = true) @PathVariable("registrationId") String registrationId,
      @ApiParam(value = "the verification id",
          required = true) @PathVariable("verificationCode") String verificationCode,
      @RequestHeader(value = "Accept", required = false) String accept) throws Exception {
    return ResponseEntity.status(HttpStatus.CREATED)
        .body(userRegistrationService.completeRegistration(registrationId, verificationCode));
  }

  @Override
  public ResponseEntity<UserRegistrationResponse> createRegistration(
      @ApiParam(value = "DBUser registration data",
          required = true) @RequestBody UserRegistrationRequest userRegistration,
      @RequestHeader(value = "Accept", required = false) String accept) throws Exception {
    return ResponseEntity.status(HttpStatus.CREATED)
        .body(userRegistrationService.initializeRegistration(userRegistration));
  }

}
