package com.loginandregistartion.controller;

import java.io.IOException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import com.loginandregistartion.api.EmailResponse;
import com.loginandregistartion.api.SendEmailApi;
import com.loginandregistartion.model.Email;
import com.loginandregistartion.services.MailerService;
import com.loginandregistartion.validator.EmailValidation;


/**
 * 'Hello World' Controller
 */
@RestController
@RequestMapping(value = "/api")
public class EmailController implements SendEmailApi {

  private MailerService mailerService;


  @Autowired
  public EmailController(MailerService mailerService, EmailValidation emailValidation) {
    this.mailerService = mailerService;
  }

  /**
   * Send email with attachments
   * 
   * @throws IOException
   */

  @Override
  @RequestMapping(value = "/send", method = RequestMethod.POST)
  public ResponseEntity<EmailResponse> sendEmail(@RequestParam("email") String email,
      @RequestParam("subject") String subject, @RequestParam("text") String text,
      @RequestParam("files") List<MultipartFile> files) throws IOException {

    Email emailAtt = new Email(email, subject, text, files);
    return new ResponseEntity<EmailResponse>(mailerService.sendEmail(emailAtt), HttpStatus.OK);
  }



}
