package com.loginandregistartion.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import com.loginandregistartion.dao.IUsersDAO;
import com.loginandregistartion.exceptions.ResourceNotFoundException;
import com.loginandregistartion.model.DBUser;
import com.loginandregistartion.security.CurrentUser;
import com.loginandregistartion.security.UserPrincipal;


@RestController
public class UserController {

  @Autowired
  private IUsersDAO usersDAO;

  @GetMapping("/user/me")
  @PreAuthorize("hasRole('USER')")
  public DBUser getCurrentUser(@CurrentUser UserPrincipal userPrincipal) {
    return usersDAO.findById(userPrincipal.getId())
        .orElseThrow(() -> new ResourceNotFoundException("User", "id", userPrincipal.getId()));
  }
}
