package com.loginandregistartion.validator;

import org.springframework.stereotype.Component;
import com.loginandregistartion.api.UserRegistrationRequest;
import com.loginandregistartion.exceptions.InvalidPasswordException;


@Component
public class UserRegistrationValidator {

  public void validateRegistrationFields(UserRegistrationRequest input) {
    validatePassword(input.getPassword());
  }

  private void validatePassword(String password) {
    if (password.isEmpty() || password.length() < 6) {
      throw new InvalidPasswordException("Invalid password.");
    }
  }

}
