package com.loginandregistartion.validator;

import org.springframework.stereotype.Component;
import com.loginandregistartion.api.ForgetPasswordVerificationInput;
import com.loginandregistartion.exceptions.InvalidPasswordException;


@Component
public class ForgetPasswordValidator {

  public void validateRegistrationFields(ForgetPasswordVerificationInput input) {
    validatePassword(input.getPassword());
  }

  private void validatePassword(String password) {
    if (password.isEmpty() || password.length() < 6) {
      throw new InvalidPasswordException("Invalid password.");
    }
  }

}
