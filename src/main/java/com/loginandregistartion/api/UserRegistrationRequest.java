package com.loginandregistartion.api;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * Response class to be returned by Api
 * 
 * @author pkmst
 *
 */
/**
 * User registration request model
 */
@ApiModel(description = "User registration request model")

@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaPKMSTServerCodegen",
    date = "2019-10-26T10:37:40.582Z")

public class UserRegistrationRequest {
  @JsonProperty("email")
  private String email = null;

  @JsonProperty("applicationUrl")
  private String applicationUrl = null;

  @JsonProperty("password")
  private String password = null;

  public UserRegistrationRequest email(String email) {
    this.email = email;
    return this;
  }

  /**
   * User's email(which will be used for email verification).
   * 
   * @return email
   **/
  @ApiModelProperty(example = "examplemail@gmail.com",
      value = "User's email(which will be used for email verification).")
  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public UserRegistrationRequest applicationUrl(String applicationUrl) {
    this.applicationUrl = applicationUrl;
    return this;
  }

  /**
   * Application url(to which email verification code will be sent).
   * 
   * @return applicationUrl
   **/
  @ApiModelProperty(value = "Application url(to which email verification code will be sent).")
  public String getApplicationUrl() {
    return applicationUrl;
  }

  public void setApplicationUrl(String applicationUrl) {
    this.applicationUrl = applicationUrl;
  }

  public UserRegistrationRequest password(String password) {
    this.password = password;
    return this;
  }

  /**
   * User's password.
   * 
   * @return password
   **/
  @ApiModelProperty(value = "User's password.")
  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    UserRegistrationRequest userRegistrationRequest = (UserRegistrationRequest) o;
    return Objects.equals(this.email, userRegistrationRequest.email)
        && Objects.equals(this.applicationUrl, userRegistrationRequest.applicationUrl)
        && Objects.equals(this.password, userRegistrationRequest.password);
  }

  @Override
  public int hashCode() {
    return Objects.hash(email, applicationUrl, password);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class UserRegistrationRequest {\n");

    sb.append("    email: ").append(toIndentedString(email)).append("\n");
    sb.append("    applicationUrl: ").append(toIndentedString(applicationUrl)).append("\n");
    sb.append("    password: ").append(toIndentedString(password)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

