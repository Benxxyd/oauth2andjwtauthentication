package com.loginandregistartion.api;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * Response class to be returned by Api
 * 
 * @author pkmst
 *
 */
/**
 * Forget password input model
 */
@ApiModel(description = "Forget password input model")

@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaPKMSTServerCodegen",
    date = "2019-11-28T17:57:52.367Z")

public class ForgetPasswordInput {
  @JsonProperty("email")
  private String email = null;

  @JsonProperty("applicationUrl")
  private String applicationUrl = null;

  public ForgetPasswordInput email(String email) {
    this.email = email;
    return this;
  }

  /**
   * User's email(which will be used for to send temporary link for forget password flow).
   * 
   * @return email
   **/
  @ApiModelProperty(example = "examplemail@gmail.com",
      value = "User's email(which will be used for to send temporary link for forget password flow).")
  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public ForgetPasswordInput applicationUrl(String applicationUrl) {
    this.applicationUrl = applicationUrl;
    return this;
  }

  /**
   * Application url(to which email verification code will be sent).
   * 
   * @return applicationUrl
   **/
  @ApiModelProperty(value = "Application url(to which email verification code will be sent).")
  public String getApplicationUrl() {
    return applicationUrl;
  }

  public void setApplicationUrl(String applicationUrl) {
    this.applicationUrl = applicationUrl;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ForgetPasswordInput forgetPasswordInput = (ForgetPasswordInput) o;
    return Objects.equals(this.email, forgetPasswordInput.email)
        && Objects.equals(this.applicationUrl, forgetPasswordInput.applicationUrl);
  }

  @Override
  public int hashCode() {
    return Objects.hash(email, applicationUrl);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ForgetPasswordInput {\n");

    sb.append("    email: ").append(toIndentedString(email)).append("\n");
    sb.append("    applicationUrl: ").append(toIndentedString(applicationUrl)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

