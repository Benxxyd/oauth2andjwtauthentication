package com.loginandregistartion.api;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * Response class to be returned by Api
 * 
 * @author pkmst
 *
 */
/**
 * User data model
 */
@ApiModel(description = "User data model")

@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaPKMSTServerCodegen",
    date = "2019-12-13T16:06:51.312Z")

public class UserData {
  @JsonProperty("id")
  private String id = null;

  @JsonProperty("email")
  private String email = null;

  @JsonProperty("name")
  private String name = null;

  @JsonProperty("imageUrl")
  private String imageUrl = null;

  @JsonProperty("roles")

  private List<String> roles = null;

  public UserData id(String id) {
    this.id = id;
    return this;
  }

  /**
   * User id.
   * 
   * @return id
   **/
  @ApiModelProperty(example = "7f3da4ce-55fa-4b95-bd0c-8489df68fbaa", value = "User id.")
  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public UserData email(String email) {
    this.email = email;
    return this;
  }

  /**
   * User's email.
   * 
   * @return email
   **/
  @ApiModelProperty(example = "example@gmail.com", value = "User's email.")
  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public UserData name(String name) {
    this.name = name;
    return this;
  }

  /**
   * User's name.
   * 
   * @return name
   **/
  @ApiModelProperty(example = "Jack Doe", value = "User's name.")
  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public UserData imageUrl(String imageUrl) {
    this.imageUrl = imageUrl;
    return this;
  }

  /**
   * URL of user's image.
   * 
   * @return imageUrl
   **/
  @ApiModelProperty(example = "/url", value = "URL of user's image.")
  public String getImageUrl() {
    return imageUrl;
  }

  public void setImageUrl(String imageUrl) {
    this.imageUrl = imageUrl;
  }

  public UserData roles(List<String> roles) {
    this.roles = roles;
    return this;
  }

  public UserData addRolesItem(String rolesItem) {
    if (this.roles == null) {
      this.roles = new ArrayList<>();
    }
    this.roles.add(rolesItem);
    return this;
  }

  /**
   * Get roles
   * 
   * @return roles
   **/
  @ApiModelProperty(value = "")
  public List<String> getRoles() {
    return roles;
  }

  public void setRoles(List<String> roles) {
    this.roles = roles;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    UserData userData = (UserData) o;
    return Objects.equals(this.id, userData.id) && Objects.equals(this.email, userData.email)
        && Objects.equals(this.name, userData.name)
        && Objects.equals(this.imageUrl, userData.imageUrl)
        && Objects.equals(this.roles, userData.roles);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, email, name, imageUrl, roles);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class UserData {\n");

    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    email: ").append(toIndentedString(email)).append("\n");
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("    imageUrl: ").append(toIndentedString(imageUrl)).append("\n");
    sb.append("    roles: ").append(toIndentedString(roles)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

