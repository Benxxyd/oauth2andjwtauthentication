package com.loginandregistartion.api;

/**
 * Response class to be returned by Api
 * 
 * @author pkmst
 *
 */
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * User state.
 */
public enum UserStateEnum {

  WAITING_FOR_EMAIL_VERIFICATION("WAITING_FOR_EMAIL_VERIFICATION"),

  ACTIVE("ACTIVE"),

  DEACTIVATED("DEACTIVATED");

  private String value;

  UserStateEnum(String value) {
    this.value = value;
  }

  @Override
  @JsonValue
  public String toString() {
    return String.valueOf(value);
  }

  @JsonCreator
  public static UserStateEnum fromValue(String text) {
    for (UserStateEnum b : UserStateEnum.values()) {
      if (String.valueOf(b.value).equals(text)) {
        return b;
      }
    }
    return null;
  }
}

