package com.loginandregistartion.api;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * Response class to be returned by Api
 * 
 * @author pkmst
 *
 */
/**
 * Forget password verification input model
 */
@ApiModel(description = "Forget password verification input model")

@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaPKMSTServerCodegen",
    date = "2019-11-28T17:36:57.547Z")

public class ForgetPasswordVerificationInput {
  @JsonProperty("password")
  private String password = null;

  public ForgetPasswordVerificationInput password(String password) {
    this.password = password;
    return this;
  }

  /**
   * User's password.
   * 
   * @return password
   **/
  @ApiModelProperty(value = "User's password.")
  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ForgetPasswordVerificationInput forgetPasswordVerificationInput =
        (ForgetPasswordVerificationInput) o;
    return Objects.equals(this.password, forgetPasswordVerificationInput.password);
  }

  @Override
  public int hashCode() {
    return Objects.hash(password);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ForgetPasswordVerificationInput {\n");

    sb.append("    password: ").append(toIndentedString(password)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

