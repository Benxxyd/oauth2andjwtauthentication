package com.loginandregistartion.api;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
/**
 * Response class to be returned by Api
 * @author pkmst
 *
 */
/**
 * DBUser registration response model
 */
@ApiModel(description = "DBUser registration response model")

@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaPKMSTServerCodegen", date = "2019-10-19T21:22:31.676Z")

public class UserRegistrationResponse   {
  @JsonProperty("id")
  private String id = null;

  @JsonProperty("userState")
  private UserStateEnum userState = null;

  public UserRegistrationResponse id(String id) {
    this.id = id;
    return this;
  }

   /**
   * Registration id.
   * @return id
  **/
  @ApiModelProperty(example = "7f3da4ce-55fa-4b95-bd0c-8489df68fbaa", value = "Registration id.")
  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public UserRegistrationResponse userState(UserStateEnum userState) {
    this.userState = userState;
    return this;
  }

   /**
   * Get userState
   * @return userState
  **/
  @ApiModelProperty(value = "")
  public UserStateEnum getUserState() {
    return userState;
  }

  public void setUserState(UserStateEnum userState) {
    this.userState = userState;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    UserRegistrationResponse userRegistrationResponse = (UserRegistrationResponse) o;
    return Objects.equals(this.id, userRegistrationResponse.id) &&
        Objects.equals(this.userState, userRegistrationResponse.userState);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, userState);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class UserRegistrationResponse {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    userState: ").append(toIndentedString(userState)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

