package com.loginandregistartion.exceptions;

public class EmailIsNotUniqueException extends RuntimeException{

    public EmailIsNotUniqueException(String message) {
        super(message);
    }
}
