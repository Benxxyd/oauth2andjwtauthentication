package com.loginandregistartion.exceptions;

public class InvalidRegistrationStateException extends RuntimeException{

    public InvalidRegistrationStateException(String message) {
        super(message);
    }
}
