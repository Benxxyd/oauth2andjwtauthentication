package com.loginandregistartion.services;

import com.loginandregistartion.api.UserRegistrationRequest;
import com.loginandregistartion.api.UserRegistrationResponse;

public interface IUserRegistrationService {

  UserRegistrationResponse initializeRegistration(UserRegistrationRequest input);

  UserRegistrationResponse completeRegistration(String registrationId, String verificationCode);

}
