package com.loginandregistartion.services;

import java.io.IOException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.loginandregistartion.api.ForgetPasswordInput;
import com.loginandregistartion.api.ForgetPasswordVerificationInput;
import com.loginandregistartion.database.UsersDatabaseService;
import com.loginandregistartion.model.Email;
import com.loginandregistartion.validator.ForgetPasswordValidator;


@Service
public class ForgetPasswordService implements IForgetPasswordService {


  @Autowired
  private UsersDatabaseService usersDatabaseService;

  @Autowired
  private ForgetPasswordValidator forgetPasswordValidator;

  @Autowired
  MailerService mailService;

  @Override
  public void initiateForgetPassword(ForgetPasswordInput forgetPasswordInput) {
    String token = usersDatabaseService.generateForgetPasswordToken(forgetPasswordInput);

    Email email = new Email(forgetPasswordInput.getEmail(), "Password change",
        buildEmailBody(forgetPasswordInput, token));
    try {
      mailService.sendEmail(email);
    } catch (IOException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
  }

  @Override
  public void confirmForgetPassword(String token,
      ForgetPasswordVerificationInput forgetPasswordVerificationInput) {
    forgetPasswordValidator.validateRegistrationFields(forgetPasswordVerificationInput);

    usersDatabaseService.changeUserPasswordWithGivenToken(forgetPasswordVerificationInput, token);
  }

  private String buildEmailBody(ForgetPasswordInput forgetPasswordInput, String token) {
    String body = "In order to finish password change, please follow next url: " + "\n"
        + forgetPasswordInput.getApplicationUrl() + "/forgetpassword?forgetPasswordToken=" + token;
    return body;
  }

}
