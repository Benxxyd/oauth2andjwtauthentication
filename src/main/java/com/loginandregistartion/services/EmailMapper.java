package com.loginandregistartion.services;

import org.springframework.stereotype.Component;
import com.loginandregistartion.api.EmailResponse;
import com.loginandregistartion.model.Email;


@Component
public class EmailMapper {

  public static EmailResponse mapToApi(Email email) {
    return new EmailResponse().email(email.getEmail()).subject(email.getSubject())
        .text(email.getText());
  }

}
