package com.loginandregistartion.services;

import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.loginandregistartion.api.UserData;
import com.loginandregistartion.dao.IUsersDAO;
import com.loginandregistartion.database.UsersDatabaseService;
import com.loginandregistartion.exceptions.ResourceNotFoundException;
import com.loginandregistartion.model.DBUser;

@Service
public class UsersService implements IUsersService {

  @Autowired
  private IUsersDAO usersDAO;

  @Autowired
  private UsersDatabaseService usersDatabaseService;


  @Override
  public UserData getUserData(String id) {
    return mapToUserData(
        usersDAO.findById(id).orElseThrow(() -> new ResourceNotFoundException("User", "id", id)));
  }

  @Override
  public void deleteUserData(String id) {
    // delete all user data
    usersDatabaseService.deleteUserData(id);
  }

  private UserData mapToUserData(DBUser user) {
    return new UserData().id(user.getId()).email(user.getEmail()).imageUrl(user.getImageUrl())
        .name(user.getName()).roles(user.getRoles().stream()
            .map(item -> item.getDBRoleEnum().authority()).collect(Collectors.toList()));
  }

}
