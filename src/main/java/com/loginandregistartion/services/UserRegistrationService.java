package com.loginandregistartion.services;

import java.io.IOException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.loginandregistartion.api.UserRegistrationRequest;
import com.loginandregistartion.api.UserRegistrationResponse;
import com.loginandregistartion.database.UsersDatabaseService;
import com.loginandregistartion.exceptions.EmailIsNotUniqueException;
import com.loginandregistartion.exceptions.InvalidRegistrationStateException;
import com.loginandregistartion.exceptions.InvalidVerificationCodeException;
import com.loginandregistartion.model.DBRegistrationData;
import com.loginandregistartion.model.DBUserStatusEnum;
import com.loginandregistartion.model.Email;
import com.loginandregistartion.validator.UserRegistrationValidator;



@Service
public class UserRegistrationService implements IUserRegistrationService {


  @Autowired
  private UsersDatabaseService usersDatabaseService;

  @Autowired
  private UserRegistrationValidator userRegistrationValidator;

  @Autowired
  MailerService mailService;

  @Override
  public UserRegistrationResponse initializeRegistration(UserRegistrationRequest input) {
    verifyThatEmailIsUnique(input.getEmail());
    userRegistrationValidator.validateRegistrationFields(input);

    UserRegistrationResponse savedRegistration = usersDatabaseService.saveRegistrationData(input);

    Email email = new Email(input.getEmail(), "Registration",
        buildEmailBody(input, usersDatabaseService.getRegistrationData(savedRegistration.getId())));
    try {
      mailService.sendEmail(email);
    } catch (IOException e) {

    }
    return savedRegistration;
  }

  @Override
  public UserRegistrationResponse completeRegistration(String registrationId,
      String verificationCode) {
    DBRegistrationData dbRegistrationData =
        usersDatabaseService.getRegistrationData(registrationId);
    validateUserRegistrationState(dbRegistrationData.getDbUserStatusEnum());
    validateVerificationCode(dbRegistrationData.getVerificationCode(), verificationCode);
    usersDatabaseService.saveUser(dbRegistrationData);
    return usersDatabaseService.updateRegistrationState(dbRegistrationData,
        DBUserStatusEnum.ACTIVE);
  }

  private void validateUserRegistrationState(DBUserStatusEnum dbUserStatusEnum) {
    if (!dbUserStatusEnum.equals(DBUserStatusEnum.WAITING_FOR_EMAIL_VERIFICATION)) {
      throw new InvalidRegistrationStateException("Invalid registration state.");
    }
  }

  private void validateVerificationCode(String dbVerificationCode, String verificationCode) {
    if (!dbVerificationCode.equals(verificationCode)) {
      throw new InvalidVerificationCodeException("Invalid verification code.");
    }
  }

  private void verifyThatEmailIsUnique(String email) {
    if (usersDatabaseService.getByUsername(email).isPresent()) {
      throw new EmailIsNotUniqueException("Email is not unique.");
    }
  }

  private String buildEmailBody(UserRegistrationRequest input,
      DBRegistrationData registrationData) {
    String body = "In order to finish registration, please follow next url: " + "\n"
        + input.getApplicationUrl() + "/registration?registrationId=" + registrationData.getId()
        + "&verificationCode=" + registrationData.getVerificationCode();
    return body;
  }

}
