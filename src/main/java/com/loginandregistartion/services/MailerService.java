package com.loginandregistartion.services;

import java.io.IOException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.loginandregistartion.api.EmailResponse;
import com.loginandregistartion.model.Email;
import com.loginandregistartion.services.mail.EmailSender;
import com.loginandregistartion.validator.EmailValidationImpl;


@Component
public class MailerService {
  private final EmailSender emailSender;

  @Autowired
  EmailValidationImpl emailValidation;


  @Autowired
  public MailerService(EmailSender emailSender) {
    this.emailSender = emailSender;
  }



  public EmailResponse sendEmail(Email email) throws IOException {
    Boolean emailValid = emailValidation.isEmailValid(email.getEmail());
    if (!emailValid) {
      throw new IllegalArgumentException("Provided email is not valid");
    }
    emailSender.sendEmail(email);
    return EmailMapper.mapToApi(email);
  }


}
