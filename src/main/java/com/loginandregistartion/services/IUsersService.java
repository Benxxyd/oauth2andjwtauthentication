package com.loginandregistartion.services;

import com.loginandregistartion.api.UserData;

public interface IUsersService {

  UserData getUserData(String id);

  void deleteUserData(String id);
}
