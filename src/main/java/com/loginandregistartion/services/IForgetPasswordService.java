package com.loginandregistartion.services;

import com.loginandregistartion.api.ForgetPasswordInput;
import com.loginandregistartion.api.ForgetPasswordVerificationInput;

public interface IForgetPasswordService {

  void initiateForgetPassword(ForgetPasswordInput forgetPasswordInput);

  void confirmForgetPassword(String token,
      ForgetPasswordVerificationInput forgetPasswordVerificationInput);

}
