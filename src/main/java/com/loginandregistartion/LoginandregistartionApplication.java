package com.loginandregistartion;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import com.loginandregistartion.config.AppProperties;

@SpringBootApplication
@EnableConfigurationProperties(AppProperties.class)
public class LoginandregistartionApplication {

  public static void main(String[] args) {
    SpringApplication.run(LoginandregistartionApplication.class, args);
  }

}
