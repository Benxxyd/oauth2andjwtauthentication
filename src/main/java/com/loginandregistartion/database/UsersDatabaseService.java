package com.loginandregistartion.database;

import java.util.ArrayList;
import java.util.Optional;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;
import com.loginandregistartion.api.ForgetPasswordInput;
import com.loginandregistartion.api.ForgetPasswordVerificationInput;
import com.loginandregistartion.api.UserRegistrationRequest;
import com.loginandregistartion.api.UserRegistrationResponse;
import com.loginandregistartion.dao.IRegistrationDAO;
import com.loginandregistartion.dao.IUserRolesDAO;
import com.loginandregistartion.dao.IUsersDAO;
import com.loginandregistartion.exceptions.RegistrationNotFoundException;
import com.loginandregistartion.model.DBAuthProvider;
import com.loginandregistartion.model.DBRegistrationData;
import com.loginandregistartion.model.DBRoleEnum;
import com.loginandregistartion.model.DBUser;
import com.loginandregistartion.model.DBUserRole;
import com.loginandregistartion.model.DBUserStatusEnum;
import com.loginandregistartion.model.UserRegistrationMapper;

@Component
public class UsersDatabaseService {

  private static final BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();

  @Autowired
  private IUsersDAO usersDAO;

  @Autowired
  private IUserRolesDAO rolesDAO;

  @Autowired
  private IRegistrationDAO registrationDAO;

  public String generateForgetPasswordToken(ForgetPasswordInput forgetPasswordInput) {
    DBUser dbUser = usersDAO.findByEmail(forgetPasswordInput.getEmail())
        .orElseThrow(() -> new UsernameNotFoundException("User not found."));

    dbUser.setForgetPasswordToken(UUID.randomUUID().toString());

    return usersDAO.save(dbUser).getForgetPasswordToken();
  }

  public void changeUserPasswordWithGivenToken(
      ForgetPasswordVerificationInput forgetPasswordVerificationInput, String token) {
    DBUser dbUser = usersDAO.findByForgetPasswordToken(token)
        .orElseThrow(() -> new UsernameNotFoundException("User not found."));

    dbUser.setPassword(encoder.encode(forgetPasswordVerificationInput.getPassword()));

    usersDAO.save(dbUser);
  }

  public UserRegistrationResponse saveRegistrationData(UserRegistrationRequest input) {
    DBRegistrationData dbRegistrationData = UserRegistrationMapper.mapToDb(input);
    dbRegistrationData = registrationDAO.save(dbRegistrationData);
    return UserRegistrationMapper.mapToApi(dbRegistrationData);
  }

  public DBRegistrationData getRegistrationData(String registrationId) {
    return registrationDAO.findById(registrationId)
        .orElseThrow(() -> new RegistrationNotFoundException("Registration not found."));
  }

  public Optional<DBUser> getByUsername(String username) {
    return this.usersDAO.findByEmail(username);
  }

  public void saveUser(DBRegistrationData dbRegistrationData) {
    DBUser dbUser = new DBUser();
    dbUser.setEmail(dbRegistrationData.getUsername());
    dbUser.setPassword(dbRegistrationData.getPassword());
    dbUser.setProvider(DBAuthProvider.local);
    dbUser = usersDAO.save(dbUser);

    DBUserRole dbUserRole = new DBUserRole();
    dbUserRole.setId(new DBUserRole.Id(dbUser.getId(), DBRoleEnum.USER));
    rolesDAO.save(dbUserRole);
  }

  public void deleteUserData(String userId) {
    DBUser dbUser = usersDAO.findById(userId)
        .orElseThrow(() -> new UsernameNotFoundException("User not found."));

    // delete registration data
    registrationDAO.deleteByUsername(dbUser.getEmail());

    // delete user role
    rolesDAO.deleteByUserId(userId);

    // delete user
    dbUser.setRoles(new ArrayList<>());
    usersDAO.delete(dbUser);
  }

  public UserRegistrationResponse updateRegistrationState(DBRegistrationData dbRegistrationData,
      DBUserStatusEnum dbUserStatusEnum) {
    dbRegistrationData.setDbUserStatusEnum(dbUserStatusEnum);

    return UserRegistrationMapper.mapToApi(registrationDAO.save(dbRegistrationData));
  }
}
