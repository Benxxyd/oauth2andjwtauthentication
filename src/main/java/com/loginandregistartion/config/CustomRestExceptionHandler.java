package com.loginandregistartion.config;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import com.loginandregistartion.exceptions.AuthMethodNotSupportedException;
import com.loginandregistartion.exceptions.EmailIsNotUniqueException;
import com.loginandregistartion.exceptions.FormatNotSupportedException;
import com.loginandregistartion.exceptions.InvalidPasswordException;
import com.loginandregistartion.exceptions.InvalidRegistrationStateException;
import com.loginandregistartion.exceptions.InvalidVerificationCodeException;
import com.loginandregistartion.exceptions.RegistrationNotFoundException;

@ControllerAdvice
public class CustomRestExceptionHandler extends ResponseEntityExceptionHandler {

  @ExceptionHandler
  public ResponseEntity<Object> handleAnyException(Exception exception) {

    // Add more mappings
    if (exception instanceof AuthMethodNotSupportedException) {
      return new ApiError(HttpStatus.METHOD_NOT_ALLOWED, exception.getMessage()).toResponse();
    }
    if (exception instanceof EmailIsNotUniqueException) {
      return new ApiError(HttpStatus.BAD_REQUEST, exception.getMessage()).toResponse();
    }
    if (exception instanceof InvalidPasswordException) {
      return new ApiError(HttpStatus.BAD_REQUEST, exception.getMessage()).toResponse();
    }
    if (exception instanceof InvalidRegistrationStateException) {
      return new ApiError(HttpStatus.BAD_REQUEST, exception.getMessage()).toResponse();
    }
    if (exception instanceof InvalidVerificationCodeException) {
      return new ApiError(HttpStatus.BAD_REQUEST, exception.getMessage()).toResponse();
    }
    if (exception instanceof FormatNotSupportedException) {
      return new ApiError(HttpStatus.BAD_REQUEST, exception.getMessage()).toResponse();
    }
    if (exception instanceof RegistrationNotFoundException) {
      return new ApiError(HttpStatus.NOT_FOUND, exception.getMessage()).toResponse();
    }
    if (exception instanceof UsernameNotFoundException) {
      return new ApiError(HttpStatus.NOT_FOUND, exception.getMessage()).toResponse();
    }

    return handleExceptionInternal(exception, null, null, null, null);
  }

  @Override
  protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
      HttpHeaders headers, HttpStatus status, WebRequest request) {
    ApiError errorDetails = new ApiError(HttpStatus.BAD_REQUEST, ex.getBindingResult().toString());
    return new ResponseEntity<Object>(errorDetails, status);
  }

}
