# Authentication and Authorization with JWT Token and Oauth2 and email service


## Setup
1. Set up application.properties as shown bellow for communication with database.
	Database used is mySql. 

	Important:* Please remove quotation marks in fields you should fill out 
```java
	spring.datasource.url = jdbc:mysql://localhost:3306/"yourDatabaseName"?useSSL=false
	spring.datasource.username = "your mysql database username"
	spring.datasource.password = "your password of mysql database"
	spring.jpa.show-sql = true
	spring.jpa.hibernate.ddl-auto = update
    
    app.mail.host=smtp.gmail.com
    app.mail.port=587
    app.mail.username=USERNAME
    app.mail.password=PASSWORD
```
---
2. Set up application.yaml file for facebook and google authorization

3. Happy using the app


## Check Swagger

To access swagger documentation type: 
`http://localhost:8080/swagger-ui.html/`

or you have contract.yaml file in com.loginandregistration.api package

Here you can check all Api Documentation.



## POSTMAN COllection
    You also have postman collection file loginAndRegistration.postman_collection.json


## Contact
Created by [@benjamin](benjamin.dedic@scaleup.ba) - feel free to contact me!