swagger: "2.0"
info:
  description: "This is a Spoken Picture BE API documentation. This documentation contains available endpoints for Spoken Picture web application"
  version: "1.0.0"
  title: "Spoken Picture"
basePath: "/v1"
tags:
- name: "registration"
  description: "Operations about user registration"
paths:
  /registrations/initiate:
    post:
      tags:
      - "registration"
      summary: "Initiate registration of new user."
      description: ""
      operationId: "createRegistration"
      consumes:
      - "application/json"
      produces:
      - "application/json"
      parameters:
      - in: "body"
        name: "userRegistration"
        description: "User registration data"
        required: true
        schema:
          $ref: "#/definitions/UserRegistrationRequest"
      responses:
        201:
          description: "User registration initiated."
          schema:
            $ref: "#/definitions/UserRegistrationResponse"
        400:
          description: "Bad request"
  '/registrations/{registrationId}/verifications/{verificationCode}/verify':
    post:
      tags:
      - "registration"
      summary: "Complete registration of new user."
      description: ""
      operationId: "completeRegistration"
      parameters:
      - $ref: '#/parameters/pathRegistrationId'
      - $ref: '#/parameters/pathVerificationCode'
      consumes:
      - "application/json"
      produces:
      - "application/json"
      responses:
        201:
          description: "User registration finished."
          schema:
            $ref: "#/definitions/UserRegistrationResponse"
        400:
          description: "Bad request"
  /forgetpassword/initiate:
    post:
      tags:
      - "forgetpassword"
      summary: "Initiate forget password flow."
      description: ""
      operationId: "initiateForgetPassword"
      consumes:
      - "application/json"
      produces:
      - "application/json"
      parameters:
      - in: "body"
        name: "forgetPasswordInput"
        description: "Forget password input."
        required: true
        schema:
          $ref: "#/definitions/ForgetPasswordInput"
      responses:
        204:
          description: "Forget password flow initiated."
        404:
          description: "User not found."
  '/forgetpassword/{forgetPasswordToken}/complete':
    post:
      tags:
      - "forgetpassword"
      summary: "Complete setting new password of user."
      description: ""
      operationId: "completeForgetPassword"
      parameters:
      - $ref: '#/parameters/pathForgetPasswordToken'
      - in: "body"
        name: "forgetPasswordVerificationInput"
        description: "Forget password verification input."
        required: true
        schema:
          $ref: "#/definitions/ForgetPasswordVerificationInput"
      consumes:
      - "application/json"
      produces:
      - "application/json"
      responses:
        204:
          description: "Password changed."
        400:
          description: "Bad request"
  
definitions:
  UserRegistrationRequest:
    description: "User registration request model"
    properties:
      email:
        type: string
        description: User's email(which will be used for email verification).
        example: examplemail@gmail.com
      applicationUrl:
        type: string
        description: Application url(to which email verification code will be sent).
      password:
        type: string
        description: User's password.
        
        
  UserRegistrationResponse:
    description: "User registration response model"
    properties:
      id:
        type: string
        description: Registration id.
        example: '7f3da4ce-55fa-4b95-bd0c-8489df68fbaa'
      userState:
        $ref: '#/definitions/UserStateEnum'

  ForgetPasswordInput:
    description: "Forget password input model"
    properties:
      email:
        type: string
        description: User's email(which will be used for to send temporary link for forget password flow).
        example: examplemail@gmail.com
      applicationUrl:
        type: string
        description: Application url(to which email verification code will be sent).

  ForgetPasswordVerificationInput:
    description: "Forget password verification input model"
    properties:
      password:
        type: string
        description: User's password.
        
  UserStateEnum:
    description: |-
      User state.
    type: string
    enum:
    - WAITING_FOR_EMAIL_VERIFICATION
    - ACTIVE
    - DEACTIVATED
    example: WAITING_FOQR_EMAIL_VERIFICATION
        
parameters:
  pathRegistrationId:
    name: registrationId
    in: path
    description: the registration id
    required: true
    type: string
  pathVerificationCode:
    name: verificationCode
    in: path
    description: the verification id
    required: true
    type: string
  pathForgetPasswordToken:
    name: forgetPasswordToken
    in: path
    description: the forget password token
    required: true
    type: string